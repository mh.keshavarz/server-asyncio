access_tokens =  []

class UnAuthenticatedError(Exception):
    pass

def check_authenticated(token):
    if token not in access_tokens:
        raise UnAuthenticatedError
